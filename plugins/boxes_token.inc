<?php

/**
 * A box of tokenized textual content.
 */
class boxes_token extends boxes_box {

  /**
   * Implementation of boxes_box::options_defaults().
   */
  public function options_defaults() {
    return array(
      'body' => '',
    );
  }

  /**
   * Implementation of boxes_box::options_form().
   */
  public function options_form(&$form_state) {
    $form = array();

    $form['body'] = array(
      '#type' => 'textarea',
      '#base_type' => 'textarea',
      '#title' => t('Box body'),
      '#default_value' => $this->options['body'],
      '#rows' => 6,
      '#description' => t('The content of the block as shown to the user.'),
    );

    $form['token_help'] = array(
      '#title' => t('Replacement Patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      //'#collapsed' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['token_help']['help'] = array(
      '#theme' => 'token_tree',
    );

    return $form;
  }

  /**
   * Implementation of boxes_box::render().
   */
  public function render() {
    $content = '';
    if (!empty($this->options['body']['value'])) {
      $content = filter_xss(token_replace($this->options['body']));
    }
    $title = isset($this->title) ? $this->title : NULL;
    return array(
      'delta' => $this->delta, // Crucial.
      'title' => $title,
      'subject' => $title,
      'content' => $content,
    );
  }
}
